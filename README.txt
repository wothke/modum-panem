modum panem

 	Copyright (C) 2017 Juergen Wothke

	
This is just a little WEBGL experiment of mine: https://www.wothke.ch/modum/#/wright-and-bastard/sets/evoke-2016
(or see https://www.youtube.com/watch?v=bx0Aakv3JPE)


The content of this project must be put into some document folder of a web server. (The "source_code" sub-folder
contains the un-minified versions.) The page plays music hosted on soundcloud: In order to use the code locally 
you'll have to put a valid soundcloud client ID into the proxy.php script.

I put the code here not so much for the benefit of anybody else but so that I have an additional 
backup of the source code.

